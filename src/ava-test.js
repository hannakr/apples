const test = require('ava');
const db = require('./api/db');

test('foo', t => {
	t.pass();
});

test('bar', async t => {
	const bar = Promise.resolve('bar');
	t.is(await bar, 'bar');
});

test('get apple', async t => {
	const appleid = "5ff93f7baa086939e0787fae";
	const { data } = await db.getOne(appleid);
	t.is(data.slug, "ariane");
})