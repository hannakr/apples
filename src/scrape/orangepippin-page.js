const fetch = require('node-fetch');
const { JSDOM } = require('jsdom');
const { readFile, writeFile } = require('fs').promises;

const lib = require('../lib');

const parseApple = (applePage) => {
    const dom = new JSDOM(applePage);
    const appleData = dom.window.document.querySelectorAll('div#varietypageattributes div.card li');
    const appleDataArray = Array.from(appleData);
    //applesElArray.shift();
    //console.log(appleDataArray[1].innerHTML);
    const applesElArray = appleDataArray.filter(element =>
        (/^(Originates|Parentage|Introduced|Uses)/.test(element.textContent)))
    // console.log(applesArray.length);
    const applesArray = applesElArray.map(element => element.textContent);
    //console.log(applesArray);
    // //console.log(applesArray[50]);
    return applesArray;

};

const cleanText = (apple) => {
    //console.log(apple);
    const attributes = [];
    for (let pieces of apple) {
        const parts = pieces.split(':');
        //console.log(parts);
        attributes.push(parts);
    }
    //console.log(attributes);
    return attributes;
}

(async () => {
    try {
        const letter = 'z'
        const applesJSON = await readFile('data/apples_' + letter + '.json');
        const applesArray = JSON.parse(applesJSON);
        //console.log(applesArray);
        for (let apple of applesArray) {
            //console.log(apple.name);
            //console.log(lib.slugify(apple.name));
            const res = await fetch(apple.link);
            const html = await res.text();
            const applesText = parseApple(html);
            const appleAttributes = cleanText(applesText);
            apple['attributes'] = appleAttributes;
            console.log(apple);
            //console.log(applesArray[0]);
        }
        await writeFile('data/apples_' + letter + '_complete.json', JSON.stringify(applesArray, null, 2));
        

    } catch(err) {
        console.log(err.message);
    }
})();
