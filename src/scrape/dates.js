const { readFile, writeFile } = require("fs").promises;

(async () => {
  const applesArray = [];
  for (let x = 97; x < 123; x++) {  //123
    const letter = String.fromCharCode(x);
    try {
      const applesJSON = await readFile(
        "data/apples_" + letter + "_cleaned.json"
      );
      const jsonArray = JSON.parse(applesJSON);
      applesArray.push(...jsonArray);
    } catch (err) {
        if (err.code !== "ENOENT") {
            console.log(err.message);
        }
    }
  }
  const datesArray = [];
  let re = /^\d{4}$/;
  let re2 = /^[1-9]{3}\ds$/;
  let re3 = /^\d{2}th century$/;
  for (let apple of applesArray) {
    //console.log(apple.parentage);
    apple["date"] = null;
    const introDate = apple.introduced;
    if (introDate && re.test(introDate)) {
        apple["date"] = introDate;
    } else if (re2.test(introDate)) {
        const decade = introDate.slice(0,introDate.length-2) + 'X';
        apple["date"] = decade;
    } else if (re3.test(introDate)) {
        const century = introDate.slice(0,2) + 'XX';
        console.log(apple.name + ': ' + century);
        apple["date"] = century;
    }
    if (!apple.parents) {
        apple["parents"] = [];
    }
    apple["area"] = null;
    apple["country"] = null;
    if (apple.origin) {
        const parts = apple.origin.split(',');
        if (parts.length > 1) {
            apple["area"] = parts[parts.length - 2].trim();
            apple["country"] = parts[parts.length - 1].trim();
        } else {
            apple["country"] = parts[parts.length -1];
        }
    }
    //console.log(apple.name, apple.area, apple.country);
    apple.description = "OP: " + apple.description
  }
  //console.log(parentsArray);
//   const singleParents = [];
//   for (let parents of parentsArray) {
//       const parts = parents.split(/ x | and | or | \+ | X /);
//       //console.log(parents);
//       //console.log(parts);
//       if (parts.length > 1) {
//           //console.log('on x ' + parts);
//           singleParents.push(...parts);
//       } else {
//         singleParents.push(parents);
//       } 
//   }
//   //singleParents.sort();
//   //console.log(singleParents);
//   const uniqueParents = [...new Set(singleParents)];
//   console.log(uniqueParents);
  await writeFile('data/apples_all_cleaned.json', JSON.stringify(applesArray, null, 2));
})();
