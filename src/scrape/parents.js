const { readFile, writeFile } = require("fs").promises;

(async () => {
  const applesArray = [];
  for (let x = 97; x < 123; x++) {
    const letter = String.fromCharCode(x);
    try {
      const applesJSON = await readFile(
        "data/apples_" + letter + "_cleaned.json"
      );
      const jsonArray = JSON.parse(applesJSON);
      applesArray.push(...jsonArray);
    } catch (err) {
        if (err.code !== "ENOENT") {
            console.log(err.message);
        }
    }
  }
  const parentsArray = [];
  let re = / x /;
  for (let apple of applesArray) {
    //console.log(apple.parentage);
    apple["parents"] = [];
    if (apple.parentage) {
        //console.log(apple.parentage);
        //parentsArray.push(apple.parentage);
        const parts = apple.parentage.split(/ x | and /);
        if (parts.length > 1) {
            apple["parents"] = parts;
        }
    }
    //console.log(apple.name, apple.parentage, apple.parents);
    apple["area"] = null;
    apple["country"] = null;
    if (apple.origin) {
        const parts = apple.origin.split(',');
        if (parts.length > 1) {
            apple["area"] = parts[parts.length - 2];
            apple["country"] = parts[parts.length - 1];
        } else {
            apple["country"] = parts[parts.length -1];
        }
    }
    console.log(apple.name, apple.area, apple.country);
  }
  //console.log(parentsArray);
//   const singleParents = [];
//   for (let parents of parentsArray) {
//       const parts = parents.split(/ x | and | or | \+ | X /);
//       //console.log(parents);
//       //console.log(parts);
//       if (parts.length > 1) {
//           //console.log('on x ' + parts);
//           singleParents.push(...parts);
//       } else {
//         singleParents.push(parents);
//       } 
//   }
//   //singleParents.sort();
//   //console.log(singleParents);
//   const uniqueParents = [...new Set(singleParents)];
//   console.log(uniqueParents);
  //await writeFile('data/apples_' + letter + '_cleaned.json', JSON.stringify(newApplesArray, null, 2));
})();
