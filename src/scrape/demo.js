const data = [[ 'Parentage', ' Unknown' ],
[ 'Originates from', ' Boston, Massachusetts, United States' ],
[ 'Introduced', ' 1750s' ],
[ 'Uses', ' Eat fresh' ],
[ 'Uses', ' Cooking' ],
[ 'Uses', ' Juice' ],
[ 'Uses', ' Drying']];

console.log(data)

// const formatData = (data) => {
//     return data.reduce((acc, [k, v]) => {
//       acc[k] = acc[k] ? [...acc[k], v] : [v];
//       return acc;
//     }, {});
// }

const appendArray = (previousValue, newValue) => {
  if (!previousValue) {
      return newValue;
  } else {
      if (Array.isArray(previousValue)) {
        return [...previousValue, newValue];
      } else {
        return [previousValue, newValue];
      }
  }
}

const formatData = (data) => {
    return data.reduce((acc, [k, v]) => {
      return {
          ...acc,
          //[k]: acc[k] ? [...acc[k], v] : [v]
          [k]: appendArray(acc[k], v)
      };
    }, {});
}

console.log(formatData(data))