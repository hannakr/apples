const test = require('ava');
const { JSDOM } = require('jsdom');

//import {parseRow} from './utils/wiki.js';
const wiki = require('./utils/wiki.js');

const getDocument = (htmlText) => {
    const { document } = (new JSDOM(htmlText)).window;
    return document;
};

//const { document } = (new JSDOM(`<table><tr><td><a href=\"/wiki/Zestar_apple\" title=\"Zestar apple\">Zestar</a></td></tr></table>`)).window;

test('parseRow should return name and link', t => {
    const document = getDocument(`<table><tr>
<td><a href=\"/wiki/Zestar_apple\" title=\"Zestar apple\">Zestar</a>
</td>
<td><a href=\"/wiki/File:Apple_Jacks_Orchard_-_Zestar_Apples_(6123001930).jpg\" class=\"image\"><img alt=\"Apple Jacks Orchard - Zestar Apples (6123001930).jpg\" src=\"//upload.wikimedia.org/wikipedia/commons/thumb/c/cb/Apple_Jacks_Orchard_-_Zestar_Apples_%286123001930%29.jpg/120px-Apple_Jacks_Orchard_-_Zestar_Apples_%286123001930%29.jpg\" decoding=\"async\" width=\"120\" height=\"79\" srcset=\"//upload.wikimedia.org/wikipedia/commons/thumb/c/cb/Apple_Jacks_Orchard_-_Zestar_Apples_%286123001930%29.jpg/180px-Apple_Jacks_Orchard_-_Zestar_Apples_%286123001930%29.jpg 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/c/cb/Apple_Jacks_Orchard_-_Zestar_Apples_%286123001930%29.jpg/240px-Apple_Jacks_Orchard_-_Zestar_Apples_%286123001930%29.jpg 2x\" data-file-width=\"4928\" data-file-height=\"3264\"></a>
</td>
<td>Minnesota, US
</td>
<td>1999
</td>
<td>Red and greenish-yellow, round, sweet and tangy, preserves well
</td>
<td>Cooking, Eating
</td>
</tr></table>`);
    const result = wiki.parseRow(document.querySelector('tr'));
    t.deepEqual(result, { name: 'Zestar', nameLink: '/wiki/Zestar_apple' });
});


test('parseRow should return name and link when name has footnote', t => {
    const document = getDocument(`(<table><tr>
<td><a href="/wiki/York_Imperial" title="York Imperial">York Imperial</a> <link rel="m
w-deduplicated-inline-style" href="mw-data:TemplateStyles:r920966791"><span class="sma
llcaps">agm</span><sup id="cite_ref-66" class="reference"><a href="#cite_note-66">[66]
</a></sup>
</td>
<td><a href="/wiki/File:York_Imperial.jpg" class="image"><img alt="York Imperial.jpg" 
src="//upload.wikimedia.org/wikipedia/commons/thumb/3/3c/York_Imperial.jpg/120px-York_
Imperial.jpg" decoding="async" width="120" height="120" srcset="//upload.wikimedia.org
/wikipedia/commons/thumb/3/3c/York_Imperial.jpg/180px-York_Imperial.jpg 1.5x, //upload
.wikimedia.org/wikipedia/commons/thumb/3/3c/York_Imperial.jpg/240px-York_Imperial.jpg 
2x" data-file-width="300" data-file-height="300"></a>
</td>
<td>York, Pennsylvania, US
</td>
<td>1820
</td>
<td>Tart yet sweet, preserves well, lop-sided shape
</td>
<td>Cider, Cooking, Eating
</td>
</tr></table>`);
    const result = wiki.parseRow(document.querySelector('tr'));
    t.deepEqual(result, { name: 'York Imperial', nameLink: '/wiki/York_Imperial' });
});

test('parseRow should return name and null when name has no link', t => {
    const document = getDocument(`(<table><tr>
<td>Wijcik McIntosh
</td>
<td>
</td>
<td>British Columbia, Canada
</td>
<td><span data-sort-value="1960&nbsp;!">Mid 1960's</span>
</td>
<td>Mutation of <a href="/wiki/McIntosh_(apple)" title="McIntosh (apple)">McIntosh app
le</a> that first showed columnar ornamental properties
</td>
<td>Eating, Cooking, Ornamental
</td>
</tr></table>`);
    const result = wiki.parseRow(document.querySelector('tr'));
    t.deepEqual(result, { name: 'Wijcik McIntosh', nameLink: null });
});

test('parseRow should get name when there is extra text', t => {
    const document = getDocument(`(<table><tr>
<td><a href="/wiki/Airlie_Red_Flesh" title="Airlie Red Flesh">Airlie Red Flesh</a> (Ne
well-Kimzey red flesh, Aerlie's Red Flesh)
</td>
<td>
</td>
<td>Oregon, US
</td>
<td><span data-sort-value="1961&nbsp;!">c. 1961</span>
</td>
<td>A large, conic apple. Light yellow-green skin strewn with white dots, occasionally
 with a faint reddish orange blush. Light pink to deep red flesh is crisp, sweet and m
ildly tart.
</td>
<td>Eating
</td>
</tr></table>`);
    const result = wiki.parseRow(document.querySelector('tr'));
    t.deepEqual(result, { name: 'Airlie Red Flesh', nameLink: '/wiki/Airlie_Red_Flesh'});
});

test('parseRow should get link when it is a new page', t => {
    const document = getDocument(`(<table><tr>
<td><a href="/w/index.php?title=Apollo_(apple)&amp;action=edit&amp;redlink=1" class="new" title="Apollo (apple) (page does not exist)">Apollo</a>
</td>
<td>
</td>
<td>
</td>
<td>
</td>
<td>Cox's Orange Pippin × Geheimrat Dr. Oldenburg
</td>
<td>Eating
</td>
</tr></table>`);
    const result = wiki.parseRow(document.querySelector('tr'));
    t.deepEqual(result, { name: 'Apollo', nameLink: null });
});

test('parseRow should get name when there is a bare footnote', t => {
    const document = getDocument(`(<table><tr>
<td><a href="/wiki/Autumn_Glory" title="Autumn Glory">Autumn Glory</a><span id="Autumn
_Glory"></span><sup id="cite_ref-6" class="reference"><a href="#cite_note-6">[6]</a></
sup>
</td>
<td>
</td>
<td>Washington, US
</td>
<td>2011
</td>
<td>The Autumn Glory variety is a hybrid of the <a href="/wiki/Fuji_(apple)" title="Fu
ji (apple)">Fuji (apple)</a> and the <a href="/wiki/Golden_Delicious" title="Golden De
licious">Golden Delicious</a> apple, featuring a red over golden background. Very swee
t, firm flesh with a subtle "cinnamon" flavour. Produced only by Domex Superfresh Grow
ers in Washington's Yakima Valley.
</td>
<td>Eating
</td>
</tr></table>`);
    const result = wiki.parseRow(document.querySelector('tr'));                       
    t.deepEqual(result, { name: 'Autumn Glory', nameLink: '/wiki/Autumn_Glory'});                          
});
