const { readFile, writeFile } = require('fs').promises;
const lib = require('../lib');

const parentsParse = (value) => {
    return value[0];
}

const placeParse = (value) => {
    return value[0];
}

const dateParse = (value) => {
    return value[0];
}

const arrayParse = (values) => {
    return values;
} 

const processTypes = {
    'parentage': parentsParse,
    'origin': placeParse,
    'introduced': dateParse,
    'uses': arrayParse,
    'alternate': arrayParse
};

const appendArray = (previousValue, newValue) => {
    if (!previousValue) {
        return newValue;
    } else {
        if (Array.isArray(previousValue)) {
          return [...previousValue, newValue];
        } else {
          return [previousValue, newValue];
        }
    }
}

const parseData = (data) => {
    const newData = {};
    for (let key in data) {
        newData[key] = processTypes[key](data[key]);
    }
    return newData;
}

//All keys - lower case, make sure they're there
//All values - remove leading space
//Originates from - change key to 'origin', remove value from array
//Parentage - remove value from array, change 'Unknown' to null
//Introduced - remove value from array
//Uses - combine multiple values into array, keep array for single values or empty array for no value
//Alternative name - change key to 'alternate',  combine multiple values into array, keep array for single values or empty array for no value
const combineData = (data) => {
    return data.reduce((acc, [key, value]) => {
      return {
          ...acc,
          //[newKey]: appendArray(acc[newKey], newValue)
          [key]: acc[key] ? [...acc[key], value] : [value]
          // [k]: appendArray(acc[k], v)
      };
    }, {});
}

const formatData = (data) => {
    console.log(data);
    return data.map(([key, value]) => {
        let newKey;
        //let newValue;
        if (key == 'Originates from') {
            newKey = 'origin';
        } else if (key == 'Alternative name') {
            newKey = 'alternate';
        } else {
            newKey = key.toLowerCase();
        }
        const newValue = value.trim() == 'Unknown' ? null : value.trim();
        //console.log(newKey, newValue);
        return [newKey, newValue];
    });
}

const jsonify = (apple) => {
    const { attributes, ...applePiece } = apple;
    const attributesArray = formatData(attributes);
    //console.log(attributesArray);
    const attributesObject = parseData(combineData(attributesArray));
    //console.log(attributesObject);
    //const attributesObject2 = parseData(attributesObject);
    console.log(attributesObject);
    const newApple = {
        ...applePiece,
        ...attributesObject
    };
    //console.log(newApple);
    return newApple;
}

const normalize = (apple) => {
    if (!('parentage' in apple)) apple['parentage'] = null;
    if (!('origin' in apple)) apple['origin'] = null;
    if (!('introduced' in apple)) apple['introduced'] = null;
    if (!('uses' in apple)) apple['uses'] = [];
    if (!('alternate' in apple)) apple['alternate'] = [];
    //console.log(apple);
    return apple;
}

(async () => {
    try {
        const letter = 'z';
        const applesJSON = await readFile('data/apples_' + letter + '_complete.json');
        const applesArray = JSON.parse(applesJSON);
        //console.log(applesArray);
        const newApplesArray = [];
        for (let apple of applesArray) {
            //console.log(apple);
            const newApple = jsonify(apple);
            const normalizedApple = normalize(newApple);
            normalizedApple['slug'] = lib.slugify(apple.name);
            console.log(normalizedApple);
            newApplesArray.push(normalizedApple);
        }
        await writeFile('data/apples_' + letter + '_cleaned.json', JSON.stringify(newApplesArray, null, 2));
        

    } catch(err) {
        console.log(err.message);
    }
})();