const fetch = require('node-fetch');
const { JSDOM } = require('jsdom');
const { writeFile } = require('fs').promises;

const parseResult = (wikiData) => {
    const dom = new JSDOM(wikiData);
    const table = dom.window.document.querySelector('table:nth-of-type(2)');
    const applesElList = table.querySelectorAll('tr');
    const applesElArray = Array.from(applesElList);
    applesElArray.shift();
    console.log(applesElArray[50].innerHTML);
    let applesArray = applesElArray.map(parseRow);
    console.log(applesArray.length);
    console.log(applesArray);
    //console.log(applesArray[50]);
    return applesArray;
};

const parseRow = (htmlEl) => {
    //console.log(htmlEl.innerHTML);
    const nameHTML = htmlEl.querySelector('td:nth-of-type(1)');
    const linkA = nameHTML.querySelector('a[href^="/wiki"]');
    const name = trimName(nameHTML.textContent.trim());
    const link = (linkA && !(linkA.getAttribute('class')=='new')) && linkA.getAttribute('href') || null;
    const countryHTML = htmlEl.querySelector('td:nth-of-type(3)');
    const country = countryHTML.textContent.trim();
    const yearHTML = htmlEl.querySelector('td:nth-of-type(4)');
    const year = yearHTML.textContent.trim();
    //console.log(year);
    return { name, link, country, year};
};

const trimName = (nameText) => {
    const name1 = nameText.replace(/ *\([^)]*\) */g, "");
    const name2 = name1.replace(/ *agm\[[0-9]*\]/g, "");
    const name3 = name2.replace(/\[[0-9]*\]/g, "");
    return name3;
};


(async () => {
    try {
        const res = await fetch('https://en.wikipedia.org/w/api.php?action=parse&page=List_of_apple_cultivars&prop=text&formatversion=2&format=json');
        const json = await res.json();
        //console.log(json.parse.text.substring(0,15));
        const applesArray = parseResult(json.parse.text);
        await writeFile('data/apples_wiki.json', JSON.stringify(applesArray, null, 2));
    } catch(err) {
        console.log(err.message);
    }
})();

module.exports.parseRow = parseRow;
