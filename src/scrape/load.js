const { readFile, writeFile } = require("fs").promises;
const db = require("../db");

(async () => {
    const applesJSON = await readFile (
        "data/apples_all_cleaned.json"
    );
    const jsonArray = JSON.parse(applesJSON);
    const jsonArrayWithDates = jsonArray.map(function (apple){
        apple['created'] = Date().toString();
        apple['modified'] = apple.created;
        return apple;
    });

    await db.dropAllData();

    const result = await db.insertAllData(jsonArray);
})();