const fetch = require('node-fetch');
const { JSDOM } = require('jsdom');
const { fstat } = require('fs');
const { writeFile } = require('fs').promises;

const parseResult = (appleData) => {
    const dom = new JSDOM(appleData);
    const list = dom.window.document.querySelector('ul.varietieslist');
    const applesElList = list.querySelectorAll('li');
    const applesElArray = Array.from(applesElList);
    //applesElArray.shift();
    //console.log(applesElArray[0].innerHTML);
    const applesArray = applesElArray.map(parseRow);
    console.log(applesArray.length);
    //console.log(applesArray);
    //console.log(applesArray[50]);
    return applesArray;
};

const parseRow = (htmlEl) => {
    //console.log(htmlEl.innerHTML);
    const linkHTML = htmlEl.querySelector('h2.varietylisttitle a[href]');
    const name = linkHTML.textContent.trim();
    const link = linkHTML.getAttribute('href');
    //console.log(name, link);
    const scoreHTML = htmlEl.querySelector('div.varietyscore-varieties meta[itemprop^="ratingValue"]');
    const score = (scoreHTML && scoreHTML.getAttribute('content')) || null;
    //console.log(score);
    const textHTML = htmlEl.querySelector('div.card-body').childNodes;
    const textArray = Array.from(textHTML);
    const description = textArray.reduce((acc, node) => {
        return (node.nodeType === 3 ? node.textContent : '');
    }, '');
    //const description2 = textArray.filter(node => node.nodeType === 3)[0].nodeValue;
    //console.log(description2);
    return { name, link, score, description};
};

const cleanLinks = (apple) => {
    console.log(apple.link);
    const cleanLink = apple.link.replace(/\.\.\/\.\./, 'https://www.orangepippin.com');
    return { ...apple, link: cleanLink };
}

const trimName = (nameText) => {
    const name1 = nameText.replace(/ *\([^)]*\) */g, "");
    const name2 = name1.replace(/ *agm\[[0-9]*\]/g, "");
    const name3 = name2.replace(/\[[0-9]*\]/g, "");
    return name3;
};

const parseApple = (applePage) => {
    const dom = new JSDOM(applePage);
    const appleData = dom.window.document.querySelectorAll('div#varietypageattributes div.card li');
    const appleDataArray = Array.from(appleData);
    //applesElArray.shift();
    //console.log(appleDataArray[1].innerHTML);
    const applesElArray = appleDataArray.filter(element =>
        (/^(Originates|Parentage|Introduced|Uses)/.test(element.textContent)))
    // console.log(applesArray.length);
    const applesArray = applesElArray.map(element => element.textContent);
    console.log(applesArray);
    // //console.log(applesArray[50]);
};


(async () => {
    try {
        const letter = 'z';
        const res = await fetch('https://www.orangepippin.com/varieties/apples/' + letter);
        const html = await res.text();
        //console.log(json.parse.text.substring(0,15));
        const appleList = parseResult(html);
        const appleData = appleList.map(cleanLinks);
        console.log(appleData);
        // write the data to the fs
        await writeFile('data/apples_' + letter + '.json', JSON.stringify(appleData, null, 2));
    
        // const res = await fetch('https://www.orangepippin.com/varieties/apples/ambrosia');
        // const html = await res.text();
        // parseApple(html);
    } catch(err) {
        console.log(err.message);
    }
})();

// for (apple of apples ){
//   await ... 
// }

module.exports.parseRow = parseRow;
