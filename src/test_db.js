const db = require('./db');
const { createTables } = require('./init');

const table_name = 'public.apples';
const text_test = `SELECT '${table_name}'::regclass`;


(async () => {
    try {
        const res = await db.query(text_test);
        console.log(res);
    } catch (err) {
        console.log(err.message);
        if (err.message == `relation "${table_name}" does not exist`) {
            console.log('no table!');
            try {
                createTables();
            } catch (err2) {
                console.log(err2.stack);
            }
        } else {
            console.log(err.stack);
        }
    }
})();

