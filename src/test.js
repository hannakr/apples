const db = require('./api/db');

test('get apple', async () => {
	const appleid = "5ff93f7baa086939e0787fae";
	const { data } = await db.getOne(appleid);
	expect(data.slug).toBe("ariane");
});