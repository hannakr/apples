const micro = require('micro');
const cors = require('micro-cors')({
  allowMethods: ['OPTIONS', 'GET', 'POST', 'PUT', 'DELETE']
});
const { router, get, post, put, del } = require('microrouter');

//const db = require('./db');
const services = require('./services');
const { ValidationError, DuplicateError } = require('./errors');
//const lib = require('./lib');

const PORT = 4042;

const server = micro(
  cors(
    router(
        get('/', (req, res) => getHome(req, res)),
        get('/api/apples', (req, res) => getHandler(req, res)),
        get('/api/apples/:appleid', (req, res) => getOneHandler(req, res)),
        post('/api/apples', (req, res) => postHandler(req, res)),
        put('/api/apples/:appleid', (req, res) => putHandler(req, res)),
        del('/api/apples/:appleid', (req, res) => deleteHandler(req, res))
    )
  )
);

// const results = {
//   data: [{ apple: 'gala' }, { apple: 'cox orange pippin' }]
// };

const getHome = async () => {
  return '<html><body><p>Hello world!</p></body></html>';
};

//using the search features:
// /api/apples?slug=slugName
// /api/apples?limit=number&skip=number
const getHandler = async (req, res) => {
    const searchTerms = {}
    //const parentSearch = {}
    //console.log(req.query);
    if ("slug" in req.query) searchTerms["slug"] = req.query.slug
    if ("country" in req.query) searchTerms["country"] = req.query.country
    if ("name" in req.query) searchTerms["name"] = req.query.name
    if ("parents" in req.query) searchTerms["parents"] = req.query.parents
    const options = {
      limit: parseInt(req.query.limit),
      skip: parseInt(req.query.skip)
    };
    try {
      const result = await services.getApples(searchTerms, options);
      return result;
    } catch (err) {
      micro.send(res, 500, err.message);
    }
};

const getOneHandler = async (req, res) => {
  const { appleid } = req.params;
  //console.log(appleid);
  try {
    //const { data } = await db.getOne(appleid);
    const result = await services.getOneApple(appleid);
    //console.log(result);
    if (!result) {
      micro.send(res, 404);
    } else {
      return result;
    }
  } catch (err) {
    console.log(err);
    micro.send(res, 500, err.message);
  }
}

const postHandler = async (req, res) => {
  // data goes in the body as json
  const data = await micro.json(req);
  try {
    const result = await services.addOneApple(data);
    //console.log('made it to the end');
    //console.log(result.ops);
    if (result.insertedCount == '1') {
      micro.send(res, 201, result); 
    } else {
      return result;
    }
  } catch (err) {
    if (err instanceof ValidationError) {
      micro.send(res, 400, err.message);
    } else if (err instanceof DuplicateError) {
      micro.send(res, 400, err.message);
    } else {
      console.log(err);
      micro.send(res, 500, err.message);
    }
  }
}

const putHandler = async (req, res) => {
  const { appleid } = req.params;
  //console.log(appleid);
  const data = await micro.json(req);
  //console.log(data);
  try {
    const result = await services.updateOneApple(appleid, data);
    //console.log(result);
    if (result.updateResult.modifiedCount == '1') {
      return result.newResult;
    } else {
      micro.send(res, 404);
    }
  } catch (err) {
    if (err instanceof DuplicateError) {
      micro.send(res, 400, err.message);
    } else {
      console.log(err);
      micro.send(res, 500, err.message);
    }
  }
}

const deleteHandler = async (req, res) => {
  const { appleid } = req.params;
  try {
    const result = await services.deleteOneApple(appleid);
    //console.log(result);
    if (result.deletedCount == '1') {
      micro.send(res, 204);
    } else if (result.deletedCount == '0') {
      micro.send(res, 404);
    } else {
      micro.send(res, 500);
    }
  } catch (err) {
    console.log(err);
    micro.send(res, 500, err.message);
  }
}

server.listen(PORT, err => {
  if (err) {
    throw err;
  }
  console.log(`> Ready on http://localhost:${PORT}`);
});
