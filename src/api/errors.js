class ValidationError extends Error {};

class DuplicateError extends Error {};

module.exports = { ValidationError, DuplicateError };