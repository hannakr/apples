const request = require('supertest');
const faker = require('faker');

const db = require('./db');
const lib = require('./lib');

let testRequest = request('http://localhost:4042');

const createApple = () => {
    const apple = {
        name: faker.commerce.productName(),
        date: faker.date.between('1800-01-01', '2020-12-31').getFullYear(),
        country: faker.address.country()
    }
    return apple;
}

// const newApple1 = {
//     name: 'Hanna\'s Apple',
//     date: '2021',
//     country: 'Finland'
// }

const newApple2 = {
    name: 'Limbertwig',
    date: '1832',
    country: 'United States'
}

const newBadApple = {
    date: '2021',
    country: 'Finland'
}

const newRepeatedApple ={
    name: 'Limbertwig',
    date: '198X',
    country: 'United Kingdom'
}

// const updateApple = {
//     name: 'Keith\'s Apple',
//     date: '2020',
//     country: 'Mexico'
// }

const addTestApple = async (newApple) => {
    const response = await testRequest.post('/api/apples')
    .send(newApple);
}

const deleteTestApple = async (appleid) => {
    const deleteResponse = await testRequest.delete(`/api/apples/${appleid}`);
}

beforeAll(() => {
    return db.createUniqueIndex();
});

afterAll(() => {
    return db.dropAllData();
});

// test1 - add an apple and make sure the success response works
// test2 - add an apple and make sure the apple is there
//actually, combine those two
// test3 - try to add an apple but it fails because you haven't provided a name
//(this hasn't been implemented yet)
// test4 - try to add an apple but it fails because 
// it's the same name as an existing apple (hasn't been implemented yet)
// test5 - delete an apple and make sure the success response works
// test6 - delete an apple that doesn't exist
// test7 - update an apple and check the response matches
// those could be sub-tests - country change matches, name changes matches and
// changes the slug, etc
// test8 - try to update an apple that doesn't exist
// test9 - try to update the name to match an apple that 
// exists with the same name
// AfterAll - drop the DB


test('adds one apple', async () => {
    //await addTestApple(newApple);
    const testApple = createApple();
    const response = await testRequest.post('/api/apples')
    .send(testApple);
    //console.log('add one apple', response.text);
    const responseText = JSON.parse(response.text);
    expect(responseText).toEqual(expect.objectContaining({
        modified: expect.any(String),
        created: expect.any(String),
        _id: expect.any(String)
    }));
    
    const slug = lib.slugify(testApple.name);
    const searchResponse = await testRequest.get(`/api/apples?slug=${slug}`);
    //console.log(searchResponse.text);
    const jsonText = JSON.parse(searchResponse.text);
    //console.log(jsonText[0].country)
    expect(jsonText).toHaveLength(1)
    expect(jsonText[0].country).toBe(testApple.country.toLowerCase())
    expect(jsonText[0].slug).toBe(`${slug}`)

    const appleid = jsonText[0]._id
    await deleteTestApple(appleid)
});

test('tries to add an apple without a name', async () => {
    const response = await testRequest.post('/api/apples')
    .send(newBadApple);

    //console.log(response);
    expect(response.status).toBe(400);
});

test('tries to add an apple with the same name as another', async () => {
    await addTestApple(newApple2);

    const response = await testRequest.post('/api/apples')
    .send(newRepeatedApple);

    //console.log(response);
    expect(response.status).toBe(400);
});

test('deletes one apple', async () => {
    const testApple = createApple();
    const response = await testRequest.post('/api/apples')
    .send(testApple);
    const responseText = JSON.parse(response.text);
    const appleid = responseText._id;

    const delResponse = await testRequest.delete(`/api/apples/${appleid}`);
    //console.log(delResponse);
    //const delResponseText = JSON.parse(delResponse.text);
    expect(delResponse.text).toBe('');

    const slug = lib.slugify(testApple.name);
    const searchResponse = await testRequest.get(`/api/apples?slug=${slug}`);
    const jsonText = JSON.parse(searchResponse.text);
    expect(jsonText).toHaveLength(0);
});

test('tries to delete an apple that doesn\'t exist', async () => {
    const appleid = '602ef89f8f7002a4c70fffff';
    const delResponse = await testRequest.delete(`/api/apples/${appleid}`);
    //console.log(delResponse);
    expect(delResponse.status).toBe(404);
    
});

test('update an apple', async () => {
    const testApple = createApple();
    await addTestApple(testApple);

    const slug = lib.slugify(testApple.name);
    const searchResponse = await testRequest.get(`/api/apples?slug=${slug}`);
    const jsonText = JSON.parse(searchResponse.text);
    const appleid = jsonText[0]._id;

    const updateApple = createApple();
    const updateResponse = await testRequest.put(`/api/apples/${appleid}`)
    .send(updateApple);

    const updateText = JSON.parse(updateResponse.text);

    expect(updateText.name).toBe(updateApple.name.toLowerCase());
    expect(updateText.date).toBe(updateApple.date);
    expect(updateText.country).toBe(updateApple.country.toLowerCase());
});

test('tries to update an apple that doesn\'t exist', async () => {
    const appleid = '602ef89f8f7002a4c70fffff';
    const testApple = createApple();

    const updateResponse = await testRequest.put(`/api/apples/${appleid}`)
    .send(testApple);

    //console.log(updateResponse);
    expect(updateResponse.status).toBe(404);
});

test('tries to update an apple to have the same name as another', async () => {
    const testApple1 = createApple();
    await addTestApple(testApple1);
    //console.log('TestApple1: ', testApple1);
    const testApple2 = createApple();
    await addTestApple(testApple2);
    //console.log('TestApple2: ', testApple2);

    const slug = lib.slugify(testApple2.name);
    const searchResponse = await testRequest.get(`/api/apples?slug=${slug}`);
    const jsonText = JSON.parse(searchResponse.text);
    const appleid = jsonText[0]._id;

    const testName1 = testApple1.name;

    const updateName = {
        name: testName1
    }
    //console.log('update: ', updateName);

    const updateResponse = await testRequest.put(`/api/apples/${appleid}`)
    .send(updateName);

    expect(updateResponse.status).toBe(400);
})
// I don't know about this test
// I need to test the response that you get when you add an apple makes sense
// I need to test that you can't add the same apple twice
// Am I going to have some required fields? 
// What happens if, say, the name field is missing?
// I need to test that the response from deleting an apple makes sense
// I need to test what happens if you try to delete an apple that doesn't exist
// I need to test the update process as well
// I need to repeat the same test but test different parts of the process each time

// this would all be easier if I could have a testing DB