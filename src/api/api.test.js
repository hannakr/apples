const {MongoClient} = require('mongodb');

describe('insert', () => {
  let connection;
  let db;

  beforeAll(async () => {
    connection = await MongoClient.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
    });
    db = await connection.db(global.__MONGO_DB_NAME__);
  });

  afterAll(async () => {
    await connection.close();
    await db.close();
  });

  it('should insert an apple into collection', async () => {
    const apples = db.collection('apples');

    const mockUser = {_id: 'some-user-id', name: "York Imperial", country: "United States", date: "1830"};
    await apples.insertOne(mockUser);

    const insertedUser = await apples.findOne({_id: 'some-user-id'});
    expect(insertedUser).toEqual(mockUser);
  });
});