const db = require('../db');
const lib = require('../lib');
const { ValidationError, DuplicateError } = require('../errors');

const processName = (name) => {
    return { 
        name: name.toLowerCase(), 
        slug: createSlug(name)
    };
}

const createSlug = (name) => {
    return lib.slugify(name);
}

const formatCountry = (country) => {
    return { country: country.toLowerCase() };
}

const formatDate = (date) => {
    return { date };
}

const formatParents = (parents) => {
    return { parents };
}

const formatMethods = {
    'name': processName,
    'slug': createSlug,
    'country': formatCountry,
    'date': formatDate,
    'parents': formatParents
};

const getApples = async (searchTerms, options) => {
    //const searchObject = {};
    // const tempSearch = { $or:
    //     [
    //         {parents: "605432f673cca3e6e241c756"}, 
    //         {parents: "605433b773cca3e6e241c75b"}
    //     ] 
    // };
    // const tempSearch2 = { parents: { '$all': ['6054337773cca3e6e241c759'] } };
    // const tempSearch3 = { name: /^golden/};
    const { name, parents, ...searchObject } = searchTerms;
    if (name) searchObject["name"] = new RegExp(`^${name}`);
    if (parents) {
        searchObject["parents"] = {};
        searchObject["parents"]['$all'] = [parents];
    }
    //console.log(searchObject);
    const data = await db.getAllData(searchObject, options);
    //console.log(data);
    return data;
}

const getOneApple = async (appleid) => {
    const data = await db.getOne(appleid);
    return data;
}

const addOneApple = async (body) => {
  //console.log(body);
  const { name,country,date } = body;
  console.log(name);
  if (!name) {
      throw new ValidationError('name is required');
  }
  //console.log(name, country, date);
  //console.log(lib.slugify(name));
  const createdDate = Date().toString();
  //const nameValues = nameProcess(name);
  const values = {
      ...processName(name),
      ...formatCountry(country),
      ...formatDate(date),
      'created': createdDate,
      'modified': createdDate
  };
  try {
    const data = await db.insertOne(values);
    return data.ops[0];
  } catch (err) {
    if (err.message.startsWith('E11000 duplicate key error collection')) {
        throw new DuplicateError('apple name already exists');
    } else {
        throw(err);
    }
  }
}

const addingParents = (oldApple, newParents) => {
    let updatedParents = [];
    if ('parents' in oldApple) {
        updatedParents = [].concat(oldApple.parents, newParents);
    } else {
        updatedParents = newParents;
    }
    return updatedParents;
}

const updateOneApple = async (appleid, data) => {
    const result = await db.getOne(appleid);
    //console.log('updateOneApple service: ', data);
    // this thing loops through the keys in data (i.e. the new values)
    // and formats each value according to the method connected to
    // the key
    // if one of the keys is 'name' then another key/value needs to be
    // added to the newValues object
    const newValues = Object.keys(data).reduce((accumulator, key) => {
        return {
            ...accumulator, 
            ...formatMethods[key](data[key])
        };
      }, {modified: Date().toString()})
    // const newValues = {
    //     'modified': Date().toString()
    // };
    // if ('name' in data) { 
    //     newValues['name'] = data.name.toLowerCase();
    //     newValues['slug'] = lib.slugify(data.name); 
    // }
    // if ('country' in data) newValues['country'] = data.country.toLowerCase();
    // if ('date' in data) newValues['date'] = data.date;
    // // do I want this? Maybe I should just replace the existing array
    // if ('parents' in data) {
    //     newValues['parents'] = addingParents(result, data.parents);
    // }
    // look at one of the functions in the scrape utility to
    // loop through the keys in the updated data and 
    // do formatting things to them based on a dictionary
    // const newValues = format({
    //     modified: Date().toString(),
    //     ...data,
    // })
    //console.log('updateOneApple service new: ', newValues);
    try {
        const updateResult = await db.updateOne(appleid, newValues);
        let newResult = {};
        if (updateResult.modifiedCount == '1') {
            newResult = await db.getOne(appleid);
        }
        return { updateResult, newResult };
    } catch (err) {
        if (err.message.startsWith('E11000 duplicate key error collection')) {
            throw new DuplicateError('apple already exists');
        } else {
            throw(err);
        }
    }
}

//this extracts just the result attribute from the result object from mongodb
//looks like { n: 1, ok: 1 } with success
const deleteOneApple = async (appleid) => {
    const deletedData = await db.deleteOne(appleid);
    return deletedData;
}

module.exports = { 
    getApples, 
    getOneApple, 
    addOneApple, 
    updateOneApple, 
    deleteOneApple 
}
