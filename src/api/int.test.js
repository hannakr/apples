const request = require('supertest');

let testRequest = request('http://localhost:4042');

test('get all the apples', async () => {
    const response = await testRequest.get('/api/apples')
    expect(response.status).toBe(200)
});

// testRequest
//     .get('/api/apples')
//     .expect('Content-Type', /json/)
//     .expect('Content-Length', '444663')
//     .expect(200)
//     .end(function(err, res){
//         if (err) throw err;
//   });

test('searches for the first 20 apples', async () => {
    const response = await testRequest.get('/api/apples?limit=20')
    expect(response.status).toBe(200)
    expect(JSON.parse(response.text)).toHaveLength(20)
});

test('searches for the apple with the slug zestar', async () => {
    const response = await testRequest.get('/api/apples?slug=zestar')
    expect(response.status).toBe(200)
});
// testRequest
//   .get('/api/apples?slug=zestar')
//   .expect('Content-Type', /json/)
//   .expect('Content-Length', '585')
//   .expect(200)
//   .end(function(err, res){
//     if (err) throw err;
// });

//is this right? Should it return 200 if the search is successful but empty?
test('searches and does not find the apple with the slug buddy', async () => {
    const response = await testRequest.get('/api/apples?slug=buddy')
    expect(response.status).toBe(200)
    expect(response.text).toHaveLength(2)
});

test('gets the apple with a specific ID', async () => {
    const response = await testRequest.get('/api/apples/5ff93f7baa086939e078825c')
    expect(response.status).toBe(200)
    expect(response.header['content-length']).toBe('600')
});
// testRequest
//   .get('/api/apple/5ff93f7baa086939e078825c')
//   .expect('Content-Type', /json/)
//   .expect('Content-Length', '600')
//   .expect(200)
//   .end(function(err, res){
//     if (err) throw err;
// });

test('detects a bad ID', async () => {
    const response = await testRequest.get('/api/apples/5ff93f7')
    expect(response.status).toBe(500)
});

test('does not find the apple with non-existant ID', async () => {
    const response = await testRequest.get('/api/apples/ffffff7baa086939efffffff')
    expect(response.status).toBe(404)
});




