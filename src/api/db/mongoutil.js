//const db = require('./db');
const { getMongoConnection, closeMongoConnection } = require('./db');

let dbName = 'apples';
let db;

if (process.env.NODE_ENV == 'test') {
    dbName = 'testapples';
}
const url = 'mongodb://localhost:27017';
const getDB = getMongoConnection(url);



const checkDB = async () => {
    if (!db) {
        db = await getDB(dbName);
    }
    return db;
}

const closeDB = async () => {
    await closeMongoConnection(url);
}

//module.exports.checkDB = checkDB;
module.exports = { checkDB, closeDB };

