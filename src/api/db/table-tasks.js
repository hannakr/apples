const mongoutil = require('./mongoutil');

const createUniqueIndex = async () => {
  //try {
    const db = await mongoutil.checkDB();
    const result = await db.collection('apples').createIndex({ slug: 1 }, { unique: true });
    //console.log(data);
    return result;
  // } catch (err) {
  //   console.log(err.stack);
  // } finally {
  //   await mongoutil.closeDB();
  // }
}

module.exports = { createUniqueIndex };

