const mongoutil = require('./mongoutil');
const { ObjectID } = require('mongodb');

const dropAllData = async () => {
  const db = await mongoutil.checkDB();
  try {
    //console.log('deleting everything');
    const data = await db.dropCollection('apples');
    //mongoutil.closeDB();
    //console.log(data);
    return data;
  } catch (err) {
    console.log(err.stack);
  } finally {
    await mongoutil.closeDB();
  }
}

const insertAllData = async (docs) => {
  try {
    //console.log('inserting new things');
    //console.log(docs);
    const db = await mongoutil.checkDB();
    const data = await db.collection('apples').insertMany(docs);
    //console.log(data);
    await mongoutil.closeDB();
    return data;
  } catch (err) {
    console.log(err.stack);
  }
}

const getAllData = async (query = {}, options = {}) => {
  try {
    const db = await mongoutil.checkDB();
    //console.log(query);
    const data = await db.collection('apples').find(query, options).toArray();
    //console.log(data);
    return data;
  } catch (err) {
    console.log(err.stack);
    return err;
  }
}

const insertOne = async (values) => {
  const db = await mongoutil.checkDB();
  const data = await db.collection('apples').insertOne(values);
  //console.log(data);
  return data;
}

const getOne = async (id) => {
  if (process.env.NODE_ENV == "test") console.log("I'm testing!");
  const db = await mongoutil.checkDB();
  //const res = await db.query(text_find_one, [id]);
  const data = await db.collection('apples').findOne({"_id": new ObjectID(id)});
  //console.log(data);
  return data;
}

const updateOne = async (id, values) => {
  const db = await mongoutil.checkDB();
  //const res = await db.query(text_update_one, values);
  const data = await db.collection('apples').updateOne({"_id": new ObjectID(id)}, { $set: values } );
  //console.log(data);
  return data; 
}

// this returns the entire result object from mongodb
const deleteOne = async (id) => {
  const db = await mongoutil.checkDB();
  const data = await db.collection('apples').deleteOne({"_id": new ObjectID(id)});
  //console.log(data);
  return data;
}

module.exports = { dropAllData, insertAllData, getAllData, insertOne, getOne, updateOne, deleteOne };
