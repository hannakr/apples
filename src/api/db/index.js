const dataTasks = require('./data-tasks');
const tableTasks = require('./table-tasks');
const mongoutil = require('./mongoutil');

module.exports = {
  getDB: mongoutil.checkDB,
  dropAllData: dataTasks.dropAllData,
  insertAllData: dataTasks.insertAllData,
  getAllData: dataTasks.getAllData,
  createUniqueIndex: tableTasks.createUniqueIndex,
  insertOne: dataTasks.insertOne,
  getOne: dataTasks.getOne,
  updateOne: dataTasks.updateOne,
  deleteOne: dataTasks.deleteOne
}
