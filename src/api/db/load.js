const db = require('./db');
const createTables = require('./init');

const text_test = 'SELECT * FROM apples';

const text_apples = 'INSERT INTO apples(name, country, date) VALUES($1, $2, $3) RETURNING *';
const values = ['spartan', 'canada', '1926'];

(async () => {
    try {
        const res = await db.query(text_apples, values);
        console.log(res.rows[0]);
    } catch (err) {
        console.log(err.stack);
    }
})();

