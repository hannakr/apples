const { MongoClient } = require('mongodb');

// Cache the Mongo DB instance by URL
//const connections = {};

let client;   //replace the connections object with a single client

const getMongoConnection = url => async name => {
  if (client && client.db(name)) {
    return client.db(name);
  }

  try {
    client = await MongoClient.connect(url);  //returns a client
    console.log(`Connected to ${url}`);

    return client.db(name);
  } catch (err) {
    console.error(err);
  }
};

const closeMongoConnection = async () => {
  try {
    client.close();
  } catch (err) {
    console.error(err);
  }
}

module.exports = { getMongoConnection, closeMongoConnection };

// const getMongoConnection

// const getDB

// const closeDB



// const MongoClient = require( 'mongodb' ).MongoClient;
// const url = "mongodb://localhost:27017";

// var _db;

// module.exports = {

//   // this connects the DB and returns a function that 
//   // just contains an error if the DB fails to connect
//   connectToServer: function( callback ) {
//     MongoClient.connect( url,  { useNewUrlParser: true }, function( err, client ) {
//       _db  = client.db('test_db');
//       return callback( err );
//     } );
//   },

//   // this actually returns the DB that was populated in the connection function
//   getDb: function() {
//     return _db;
//   }
// };