const textTasks = require('./text-tasks');

module.exports = {
  slugify: textTasks.slugify
}
