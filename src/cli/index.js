//@ts-check
const { prompt } = require('enquirer');
//const c = require('ansi-colors');
const { newInput, navigate, newSearch } = require('./prompts');
const { getApples, 
    getOneApple, 
    updateAppleData, 
    sendAppleData, 
    deleteOneApple } = require('./api');


const compare = (oldData, newData) => {
    const newKeys = Object.keys(newData);
    const updateData = {}
    for (let key of newKeys) {
        if (oldData[key] !== newData[key]) {
            updateData[key] = newData[key];
        }
    }
    return updateData;
}

const formatResults = (results) => {
    for (let apple of results) {
        console.log(apple.name);
    }
}

const getMoreApples = async (count) => {
    let skip = count * 20;
    const results = await getApples(skip);
    formatResults(results);
    let pageSelect = navigate();
    const answer = await pageSelect.run();
    if (results.length == 20 && answer == 'forward') {
        console.log('get more');
        await getMoreApples(count+1);
    } else if (answer == 'back' && count > 0) {
        console.log('go back');
        await getMoreApples(count-1);
    }
}

const getAllApples = async () => {
    try {
        await getMoreApples(0);
    } catch (err) {
        console.error(err);
    }
}

const findApple = () => {
    console.log('Find an apple');
}

const createApple = async () => {
    //console.log('Enter attributes');
    try {
        //console.log(typeof newAppleData);
        const newPrompt = newInput();
        const values = await newPrompt.run();
        console.log(values);
        sendAppleData(values);
    } catch (err) {
        console.error(err);
    }
}

const editApple = async () => {
    //console.log('Enter ID to update');
    try {
        const searchPrompt = newSearch();
        const query = await searchPrompt.run();
        console.log(query);
        const values = await getOneApple(query);
        //console.log(c.green.bold('Name: '), values['name']);
        //console.log(c.green.bold('Country: '), values.country);
        //console.log(c.green.bold('Date: '), values.date);
        const editPrompt = newInput(values);
        const editResult = await editPrompt.run();
        //console.log(editResult);
        const changedResult = compare(values, editResult);
        await updateAppleData(values._id, changedResult);
    } catch (err) {
        console.error(err);
    }
}

const destroyApple = async () => {
    //console.log('Enter ID to destroy');
    try {
        const searchPrompt = newSearch();
        const query = await searchPrompt.run();
        console.log(query);
        await deleteOneApple(query);
    } catch (err) {
        console.log('error in destroy');
        console.error(err);
    }
}

const actions = {
    'Get all apples': getAllApples,
    'Find an apple': findApple,
    'Add an apple': createApple,
    'Edit an apple': editApple,
    'Delete an apple': destroyApple,
};

const getChoice = async () => {
    const response = await prompt({
        type: 'select',
        name: 'action',
        message: 'What would you like to do?',
        choices: ['Get all apples', 'Find an apple', 'Add an apple', 'Edit an apple', 'Delete an apple', 'Exit']
    });
    console.log(response);
    //console.log(actions[response.action]);
    if (response.action == 'Exit') {
        process.on('exit', function() {
            return console.log('Enjoy your apples!');
        });
    } else {
        await actions[response.action]();
        await getChoice();
    }
}

getChoice();
