const superagent = require('superagent');

const getApples = async (skip) => {
    try {
        const res = await superagent.get(`http://localhost:4042/api/apples?limit=20&skip=${skip}`);
        return JSON.parse(res.text);
    } catch (err) {
        console.error(err);
    }
}

const getOneApple = async (id) => {
    try {
        const res = await superagent.get(`http://localhost:4042/api/apples/${id}`);
        console.log(res.text);
        return JSON.parse(res.text);
    } catch (err) {
        console.error(err);
    }
}

const updateAppleData = async (id, values) => {
    try {
        const res = await superagent
            .put(`http://localhost:4042/api/apples/${id}`)
            .send(values);
    } catch (err) {
        console.error(err);
    }
}

const sendAppleData = async (values) => {
    const { parent, ...subset } = values;
    try {
        const res = await superagent
            .post('http://localhost:4042/api/apples')
            .send(subset);
    } catch (err) {
        console.error(err);
    }
}

const deleteOneApple = async (id) => {
    try {
        const res = await superagent.delete(`http://localhost:4042/api/apples/${id}`);
        //console.log(res);
    } catch (err) {
        console.dir(err, { depth: 1});
        //console.log(err.status, )
        //console.error(err);
    }
}

module.exports = {
    getApples,
    getOneApple,
    updateAppleData,
    sendAppleData,
    deleteOneApple
}