const { prompt, Form, Input } = require('enquirer');

const { hSelect } = require('./hselect');

const newInput = (initial = {}) => {
    const appleData = new Form({
        name: 'appledata',
        message: 'Add or edit data',
        choices: [
            { name: 'name', message: 'Apple name', initial: initial.name ? initial.name : ''},
            { name: 'country', message: 'Origin country', initial: initial.country ? initial.country : ''},
            { name: 'date', message: 'Date introduced', initial: initial.date ? initial.date : ''},
            { name: 'parents', message: 'Parents', initial: initial.parents ? initial.parents: ''}
        ]
    });
    return appleData;
}

const navigate = () => {
    const pageSelect = new hSelect({
        message: 'Navigate:',
        choices: ['forward', 'back', 'exit']
    });
    return pageSelect;
}

const newSearch = () => {
    const searchPrompt = new Input({
        message: 'Enter search term',
    });
    return searchPrompt;
}

module.exports = {
    newInput,
    navigate,
    newSearch
}

